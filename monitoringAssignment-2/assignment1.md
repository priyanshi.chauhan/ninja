## Assignment 2

#### Tasks

### Must do
Continuing from Day 1..........

- Install grafana 
- Add prometheus as data source in grafana.
- Add hostnames to your instances where your software component is installed.
- Make different dashboards for
    - Your Component
      - should contain general metrics widget for your software component.
      - your widgets should change according to your hostname.
      - you should be able to select multiple hostnames from dropdown.
    - your node
      - your widgets should change according to your hostname.
      - should contain general metrics widget like CPU,memory,disk space etc...
- Import a Node Exporter Dashboard and make modifications in visualisation to get familiar with it.

#### Solution

###### Installed Prometheus, Node Exporter and Nexus Exporter.

- Prometheus

<img src="./snapshot/1.png">
<img src="./snapshot/2.png">

- Node Exporter

<img src="./snapshot/3.png">
<img src="./snapshot/6.png">
<img src="./snapshot/7.png">

- Nexus Exporter

<img src="./snapshot/4.png">
<img src="./snapshot/5.png">

##### Modified Prometheus.yml file

```
$ vi /etc/prometheus/prometheus.yml
```
<img src="./snapshot/8.png">

##### Prometheus Targets

<img src="./snapshot/9.png">

##### PromQL Queries

<img src="./snapshot/10.png">
<img src="./snapshot/11.png">
<img src="./snapshot/12.png">
<img src="./snapshot/13.png">

#### Grafana Dashboard

###### Nexus Exporter Dashboard

<img src="./snapshot/1.2.png">

###### Node Exporter Dashboard

<img src="./snapshot/1.1.png">

## Author
- [@Priyanshi.Chauhan] [priyanshi.chauhan@mygurukulam.org]
