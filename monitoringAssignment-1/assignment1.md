## Assignment 1

#### Tasks

### Must do
- Install Prometheus on a Linux server using any of the available ways. (Preferably from source)
- Setup cluster or HA for your Software component using ansible role which was created earlier in your ansible assignments. Now use custom exporter for your database/middleware/software-component which is assigned to you (like mongo-exporter/mysql-exporter) or if not so use node exporter)
- Try Exploring custom metrics using prometheus and try custom promql queries for options like:
     - available disk space of the node
     - CPU cores
     - CPU utilization
     - Memory utilization
     - uptime
     - other metrices related to your software

#### Solution

###### Installed Prometheus, Node Exporter and Nexus Exporter.

- Prometheus

<img src="./snapshot/1.png">
<img src="./snapshot/2.png">

- Node Exporter

<img src="./snapshot/3.png">
<img src="./snapshot/6.png">
<img src="./snapshot/7.png">

- Nexus Exporter

<img src="./snapshot/4.png">
<img src="./snapshot/5.png">

##### Modified Prometheus.yml file

```
$ vi /etc/prometheus/prometheus.yml
```
<img src="./snapshot/8.png">

##### Prometheus Targets

<img src="./snapshot/9.png">

##### PromQL Queries

<img src="./snapshot/10.png">
<img src="./snapshot/11.png">
<img src="./snapshot/12.png">
<img src="./snapshot/13.png">


## Author
- [@Priyanshi.Chauhan] [priyanshi.chauhan@mygurukulam.org]
